# Chatty-Web for Ferdium
To install, copy the files from this repo to `~/.config/Ferdium/recipes/dev/chatty-web/`.
Update the placeholder in `package.json` to point to the hostname or IP of your phone.
