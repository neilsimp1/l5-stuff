# Chatty-Web
This is a simple script + yad dialog that will run Chatty through [GTK Broadway](https://docs.gtk.org/gtk4/broadway.html).

# How to install
1. Copy `chatty-web.desktop` to `~/.local/share/applications`.
2. Copy `chatty-web.sh` to `~/.local/bin/`.

# Known issues
- While this is running, Chatty will not open on your phone.
- Copy/paste in to or out of the Broadway web view doesn't work. (It *works* but it's copy/pasting on the host device (the phone)).
- It crashes sometimes. When you first load it up, give it a little time to load before using it.
