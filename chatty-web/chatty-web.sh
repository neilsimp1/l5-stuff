#!/usr/bin/env sh

HOSTNAME=$(cat /etc/hostname)

kill_chatty() {
	killall chatty
	killall chatty
}

kill_chatty

nohup broadwayd :5 &
GDK_BACKEND=broadway BROADWAY_DISPLAY=:5 nohup chatty &

yad --title="chatty-web" \
	--borders=15 \
	--text="Chatty is running on http://$HOSTNAME:8085." \
	--button="Close"

kill_chatty

nohup chatty --daemon &
